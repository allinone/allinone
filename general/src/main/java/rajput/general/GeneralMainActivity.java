package rajput.general;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import rajput.sheelendar.MainActivity;

public class GeneralMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.general_activity_main);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }
}
