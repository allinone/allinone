package rajput.sheelendar;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sheelendar on 05/01/17.
 */

public class TransactionRetrieveRecycleAdapter extends RecyclerView.Adapter<TransactionRetrieveRecycleAdapter.BindingHolder> {

    RecyclerAndCoordinatingActivity activity;
    private List<String> paymentInfoList;

    public TransactionRetrieveRecycleAdapter(RecyclerAndCoordinatingActivity activity, List<String> paymentInfoList) {
        this.activity = activity;
        this.paymentInfoList = paymentInfoList;
    }


    public static class BindingHolder extends RecyclerView.ViewHolder {
        //private ViewDataBinding binding;

        TextView tvReference;
        TextView tvProduct;

        TextView tvDate;
        TextView tvTotal;
        TextView tvBalance;
        TextView tvAmount;
        TextView tvTds;
        TextView tvMarkup;
        TextView tvMedium;
        TextView tvDesc;
        TextView textView;
        // ViewDataBinding binding;

        public BindingHolder(View rowView) {


            super(rowView);

            textView = (TextView) rowView.findViewById(R.id.user);
            //  binding = DataBindingUtil.bind(rowView);
           /* tvReference = (TextView) rowView.findViewById(R.id.tvReference);
            tvProduct = (TextView) rowView.findViewById(R.id.tvProduct);
            tvDate = (TextView) rowView.findViewById(R.id.tvDate);
            tvMedium = (TextView) rowView.findViewById(R.id.tvMedium);
            tvBalance = (TextView) rowView.findViewById(R.id.tvBalance);
            tvTds = (TextView) rowView.findViewById(R.id.tvTds);
            tvMarkup = (TextView) rowView.findViewById(R.id.tvMarkup);
            tvAmount = (TextView) rowView.findViewById(R.id.tvAmount);
            tvDesc = (TextView) rowView.findViewById(R.id.tvDesc);*/
        }

       /* public ViewDataBinding getBinding() {

            return binding;
        }*/
    }

    @Override
    public TransactionRetrieveRecycleAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(activity).inflate(R.layout.transaction_list_view_item, parent, false);

        return new BindingHolder(view);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {

        holder.textView.setText(paymentInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return paymentInfoList.size();
    }
}
