package rajput.sheelendar;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import rajput.sheelendar.base.BaseDefaultActivity;
import rajput.sheelendar.databinding.TransactionRetrieveBinding;


/**
 * Created by sheelendar on 09/01/17.
 */

public class RecyclerAndCoordinatingActivity extends BaseDefaultActivity {


    /**
     * Created by sheelendar on 04/01/17.
     */


    public TransactionRetrieveBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.transaction_retrieve);

        //  CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(android.R.style.TextAppearance);
        binding.collapsingToolbar.setExpandedTitleTextAppearance(android.R.style.TextAppearance);
        binding.collapsingToolbar.setTitle("Transactions");

        //binding.collapseToolbar

        binding.recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recycler.setLayoutManager(layoutManager);

        new TransactionRetrieveHandler(this);

    }
}
