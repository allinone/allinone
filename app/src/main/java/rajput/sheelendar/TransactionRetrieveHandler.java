package rajput.sheelendar;

import java.util.ArrayList;

/**
 * Created by sheelendar on 04/01/17.
 */

public class TransactionRetrieveHandler {

    private final RecyclerAndCoordinatingActivity activity;


    public TransactionRetrieveHandler(final RecyclerAndCoordinatingActivity activity) {
        this.activity = activity;
        ArrayList arrayList = new ArrayList<String>();
        for (int i = 0; i < 50; i++) {
            arrayList.add("sheelendar" + i);
        }
        TransactionRetrieveRecycleAdapter adapter = new TransactionRetrieveRecycleAdapter(activity, arrayList);
        activity.binding.recycler.setAdapter(adapter);
    }
}
