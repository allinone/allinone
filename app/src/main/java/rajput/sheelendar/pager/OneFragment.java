package rajput.sheelendar.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rajput.sheelendar.R;

/**
 * Created by sheelendar on 14/01/17.
 */

public class OneFragment extends DefaultFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.one_fragment, container, false);
        TextView name = (TextView) view.findViewById(R.id.name);
        name.setText("one fragment");
        return view;


    }
}

