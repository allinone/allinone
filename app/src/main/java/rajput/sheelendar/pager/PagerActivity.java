package rajput.sheelendar.pager;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import rajput.sheelendar.R;
import rajput.sheelendar.base.BaseDefaultActivity;
import rajput.sheelendar.databinding.ActivityPagerBinding;

/**
 * Created by sheelendar on 12/01/17.
 */

public class PagerActivity extends BaseDefaultActivity {
    ActivityPagerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pager);

        //  CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        binding.collapsingToolbar.setCollapsedTitleTextAppearance(android.R.style.TextAppearance);
        binding.collapsingToolbar.setExpandedTitleTextAppearance(android.R.style.TextAppearance_Large);
        binding.collapsingToolbar.setTitle("Pager View And Tab Layout");

        binding.viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        binding.tabs.setupWithViewPager(binding.viewPager);

    }
}
