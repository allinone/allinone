package base;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import rajput.allinone.R;

public class BaseDefaultActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onBackPressed() {
        try {
            finish();
        } catch (Exception e) {

        }
    }

    /*  public void showProgressbar() {
            if (progressBar == null) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                progressBar = new ProgressBar(this);
                progressBar.setLayoutParams(layoutParams);
                LinearLayout test = new LinearLayout(getApplicationContext());
                test.addView(progressBar, 20, 20);
                //     progressBar.setProgressDrawable(this.getResources().getDrawable(R.drawable.progress_bar_circle));

                //    ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, 500); // see this max value coming back here, we animale towards that value
                //      animation.setDuration(5000); //in milliseconds
                //    animation.setInterpolator(new DecelerateInterpolator());
                //  animation.start();
            }
            progressBar.setVisibility(View.VISIBLE);
        }

        public void hideProgressBar() {
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }*/
    public void hideProgressBar() {

    }

    public void showProgressBar() {
    }

}
