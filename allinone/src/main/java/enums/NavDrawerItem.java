package enums;

import android.support.annotation.StringRes;

import rajput.allinone.R;

/**
 * Created by sheelendar on 07/03/17.
 */

public class NavDrawerItem {

    public enum DRAWER {
        MAIN_FRAGMENT(R.string.home, true) {
            @Override
            public void execute() {

            }
        };


        public int title;
        public boolean isVisible;

        private DRAWER(@StringRes int name, boolean isVisible) {
            this.title = name;
            this.isVisible = isVisible;

        }

        public abstract void execute();
    }
}
