package Common;

/**
 * Created by mohan on 04/02/16.
 */

import com.google.android.gms.tagmanager.Container;
import com.google.android.gms.tagmanager.ContainerHolder;

/**
 * Singleton to hold the GTM Container (since it should be only created once
 * per run of the app).
 */
public class ContainerHolderSingleton {
    private static ContainerHolder containerHolder;

    /**
     * Utility class; don't instantiate.
     */
    private ContainerHolderSingleton() {
    }

    public static ContainerHolder getContainerHolder() {
        return containerHolder;
    }

    public static Container getContainer() {
        return containerHolder != null ? containerHolder.getContainer() : null;
    }

    public static void setContainerHolder(ContainerHolder c) {
        containerHolder = c;
    }
}
