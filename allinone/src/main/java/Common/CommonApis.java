package Common;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tagmanager.ContainerHolder;
import com.google.android.gms.tagmanager.TagManager;

import java.util.concurrent.TimeUnit;

import base.BaseDefaultActivity;
import rajput.allinone.R;


public class CommonApis {

    public static void execute(BaseDefaultActivity activity, String containerId, int defaultContainer) {

        getGTMContainer(activity, containerId, defaultContainer);

    }

    public static void getGTMContainer(final BaseDefaultActivity activity, String containerId, int defaultContainerId) {
        TagManager tagManager = TagManager.getInstance(activity.getApplicationContext());
        PendingResult<ContainerHolder> pending =
                tagManager.loadContainerPreferNonDefault(containerId,
                        R.raw.allinone_container);

        pending.setResultCallback(new ResultCallback<ContainerHolder>() {
            @Override
            public void onResult(ContainerHolder containerHolder) {
                if (containerHolder != null && containerHolder.getContainer() != null) {
                    containerHolder.refresh();
                    ContainerHolderSingleton.setContainerHolder(containerHolder);
                    update(activity);
                }
            }
        }, 3, TimeUnit.SECONDS);
    }

    private static void update(BaseDefaultActivity activity) {
    }

}
