package Common;

import android.content.Context;

import com.google.android.gms.tagmanager.Container;

import util.StringUtil;

public class KeyValueDatabase {


    public static String getValueFor(Context context, DatabaseKeys.KEYS key) {
        return getValueFor(context, key.name());
    }


    public static String getValueFor(Context context, String keyName) {
        if (context == null) {
            return null;
        }
        Container container = ContainerHolderSingleton.getContainer();
        if (container != null && !StringUtil.isNullOrEmpty(container.getString(keyName.toLowerCase()))) {
            return container.getString(keyName.toLowerCase());
        }

        int identifier = context.getResources().getIdentifier(
                keyName.toLowerCase(), "string", context.getPackageName());
        if (identifier > 0) {
            return context.getResources().getString(identifier);
        }
        return "";
    }

    public static String getValueFormGTM(Context context, String keyName) {
        if (context == null) {
            return null;
        }
        Container container = ContainerHolderSingleton.getContainer();
        if (container != null && !StringUtil.isNullOrEmpty(container.getString(keyName.toLowerCase()))) {
            return container.getString(keyName.toLowerCase());
        }
        return "";
    }

}

