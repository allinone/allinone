package rajput.allinone;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.TextView;

import Common.DatabaseKeys;
import Common.KeyValueDatabase;
import pojos.WebCategory;
import rajput.allinone.allinonefragemnt.AboutAllInOneFragment;
import rajput.allinone.allinonefragemnt.CustomSearchFrament;
import rajput.allinone.allinonefragemnt.SendFeedBackFragment;
import rajput.allinone.allinonefragemnt.SingleCategoryFragment;
import rajput.allinone.allinonefragemnt.WebCategoryFragment;
import util.Constants;
import util.ContextUtil;
import util.Util;

/**
 * Created by sheelendar on 28/02/17.
 */

public class OnNavigationHandler implements NavigationView.OnNavigationItemSelectedListener {
    private CategoryActivity activity;
    private DrawerLayout drawer;


    public OnNavigationHandler(CategoryActivity activity) {
        this.activity = activity;
        initialize();
    }

    private void initialize() {

        TextView navDrawerMessage = (TextView) activity.navigationView.getHeaderView(0).
                findViewById(R.id.navDrawerMessage);
        navDrawerMessage.setText(KeyValueDatabase.getValueFor(activity, DatabaseKeys.KEYS.nav_drawer_message));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);

        if (activity.webCategoryHashMap == null) {

            return true;
        }
        WebCategory webCategory = null;
        Fragment singleCategoryFragment = new SingleCategoryFragment();
        switch (id) {

            case R.id.home:

                WebCategoryFragment webCategoryFragment = new WebCategoryFragment();
                activity.reloadFragment(webCategoryFragment);
                drawer.closeDrawer(GravityCompat.START);

                return true;

            case R.id.nav_shopping:
                webCategory = activity.webCategoryHashMap.get("Shopping");
                startFragment(singleCategoryFragment, webCategory);
                break;
            case R.id.nav_recharge:
                webCategory = activity.webCategoryHashMap.get("Recharge");
                startFragment(singleCategoryFragment, webCategory);
                break;
            case R.id.nav_movie:
                webCategory = activity.webCategoryHashMap.get("Movie");
                startFragment(singleCategoryFragment, webCategory);
                break;
            case R.id.nav_travel:
                webCategory = activity.webCategoryHashMap.get("Travel");
                startFragment(singleCategoryFragment, webCategory);
                break;
            case R.id.nav_food:
                webCategory = activity.webCategoryHashMap.get("Food");
                startFragment(singleCategoryFragment, webCategory);
                break;
            case R.id.nav_social:
                webCategory = activity.webCategoryHashMap.get("Social");
                startFragment(singleCategoryFragment, webCategory);
                break;
            case R.id.nav_send:
                singleCategoryFragment = SendFeedBackFragment.newInstance("", "");
                startFragment(singleCategoryFragment, null);
                break;
            case R.id.nav_share:
                ContextUtil.shareData(activity, activity.getString(R.string.app_share_email_header),
                        KeyValueDatabase.getValueFor(activity, DatabaseKeys.KEYS.app_invites_friends_message) +
                                "\n\n" + ContextUtil.getAppUrl(activity, ""));
                break;
            case R.id.nav_all_in_one:
                singleCategoryFragment = AboutAllInOneFragment.newInstance("", "");
                startFragment(singleCategoryFragment, null);
                break;
            case R.id.nav_sarch:
                singleCategoryFragment = CustomSearchFrament.newInstance("", "");
                startFragment(singleCategoryFragment, null);
                break;
            case R.id.nav_rate_us:
                rateUsGreat();
                break;
            case R.id.more:
                webCategory = activity.otherWebCategory;
                startFragment(singleCategoryFragment, webCategory);
                break;
        }

        return true;
    }

    public void startFragment(Fragment singleCategoryFragment, WebCategory webCategory) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.WEB_LIST, Util.getJson(webCategory));
        singleCategoryFragment.setArguments(bundle);
        activity.reloadFragment(singleCategoryFragment);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void rateUsGreat() {
        Uri uri = Uri.parse(ContextUtil.getAppUrl(activity, ""));
        Intent rateUsIntent = new Intent(Intent.ACTION_VIEW, uri);
        rateUsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            activity.startActivity(rateUsIntent);

        } catch (ActivityNotFoundException e) {
            ContextUtil.showSnackBar(activity, "Couldn't launch the market");
        }
    }

}
