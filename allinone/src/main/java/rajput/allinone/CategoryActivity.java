package rajput.allinone;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashMap;

import Common.CommonApis;
import Common.DatabaseKeys;
import Common.KeyValueDatabase;
import base.BaseDefaultActivity;
import pojos.WebCategory;
import pojos.WebLinkInstance;
import pojos.WebResponseObject;
import rajput.allinone.allinonefragemnt.MainFragment;
import rajput.allinone.allinonefragemnt.SingleCategoryFragment;
import rajput.allinone.allinonefragemnt.WebCategoryFragment;
import util.Constants;
import util.ContextUtil;

public class CategoryActivity extends BaseDefaultActivity implements SingleCategoryFragment.OnListFragmentInteractionListener {

    public FireBaseHandler fireBaseHandler;
    private Fragment mainFragment = null;
    public WebResponseObject baseResponseObject;
    public WebCategory otherWebCategory;
    public NavigationView navigationView;
    public HashMap<String, WebCategory> webCategoryHashMap;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        setSupportActionBar(toolbar);
        if (!ContextUtil.isNetworkAvailable(this, checkInternetOnClickListener)) {
            return;
        }
        CommonApis.execute(this, Constants.ALLINONE_CONTAINER_ID, R.raw.allinone_container);
        showProgressbar();
        showShareFabButton();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new OnNavigationHandler(this));
        navigationView.setItemIconTintList(null);
        reloadFragment(null);
        fireBaseHandler = new FireBaseHandler(this);
        fireBaseHandler.loadFireBaseForm(Constants.MAIN_WEB_CATEGORIES);
        fireBaseHandler.loadFireBaseForm(Constants.OTHER_WEB_CATEGORIES);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        if (mainFragment == null || mainFragment instanceof MainFragment ||
                mainFragment instanceof WebCategoryFragment) {
            super.onBackPressed();
            return;
        }
        if (mainFragment != null) {
            reloadFragment(new WebCategoryFragment());
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.search).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void reloadFragment(Fragment fragment) {

        if (fragment == null) {
            fragment = new MainFragment();
        }
        mainFragment = fragment;
            /*fragment = new MainFragment();
            mainFragment = (MainFragment) fragment;*/

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.replace(R.id.frame_layout, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

    }

    public void showProgressbar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void showShareFabButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public void onListFragmentInteraction(WebLinkInstance item) {
        hideProgressBar();
    }

    DialogInterface.OnClickListener checkInternetOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            finish();
        }
    };
}
