package rajput.allinone.allinonefragemnt;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rajput.allinone.CategoryActivity;
import rajput.allinone.R;

/**
 * Created by sheelendar on 07/03/17.
 */

public class WebCategoryFragment extends Fragment {

    private CategoryActivity activity;
    private WebCategoryFragmentHandler holidayHomePageHandler;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.web_category_fragment,
                container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        activity = (CategoryActivity) getActivity();
        super.onActivityCreated(savedInstanceState);
        holidayHomePageHandler = new WebCategoryFragmentHandler(activity, view);
        holidayHomePageHandler.initialize();

    }

}
