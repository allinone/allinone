package rajput.allinone.allinonefragemnt;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import pojos.WebCategory;
import pojos.WebLinkInstance;
import rajput.allinone.MyItemRecyclerViewAdapter;
import rajput.allinone.R;
import util.Constants;
import util.Util;
import webview.WebViewActivity;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SingleCategoryFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private WebCategory webCategory;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SingleCategoryFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SingleCategoryFragment newInstance(int columnCount) {
        SingleCategoryFragment fragment = new SingleCategoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.single_category_fragment, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            webCategory = Util.parseJson(bundle.getString(Constants.WEB_LIST), WebCategory.class);
        }
        // Set the adapter
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.webCategoryList);
        if (webCategory != null) {
            mListener = new OnListFragmentInteractionListener() {
                @Override
                public void onListFragmentInteraction(WebLinkInstance item) {
                    openWebViewActivity(item);
                }
            };
            Context context = view.getContext();
            mColumnCount = webCategory.getWebLinkInstances().size();
            LinearLayoutManager verticalLayoutmanager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(verticalLayoutmanager);
            recyclerView.setAdapter(new MyItemRecyclerViewAdapter(webCategory.getWebLinkInstances(), mListener, getActivity()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(WebLinkInstance item);

    }

    private void openWebViewActivity(WebLinkInstance webLinkInstance) {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(webLinkInstance);
        intent.putExtra("webLink", json);
        getActivity().startActivity(intent);
    }
}
