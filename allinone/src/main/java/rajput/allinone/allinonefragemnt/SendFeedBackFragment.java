package rajput.allinone.allinonefragemnt;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import base.BaseDefaultActivity;
import rajput.allinone.R;
import util.ContextUtil;
import util.StringUtil;


public class SendFeedBackFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View view;
    private EditText etSendFeedback;
    private Button btFeedback;
    private BaseDefaultActivity activity;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SendFeedBackFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SendFeedBackFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SendFeedBackFragment newInstance(String param1, String param2) {
        SendFeedBackFragment fragment = new SendFeedBackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (BaseDefaultActivity) getActivity();
        btFeedback = (Button) view.findViewById(R.id.btFeedback);
        etSendFeedback = (EditText) view.findViewById(R.id.etSendFeedback);
        btFeedback.setOnClickListener(sendFeedbackOnClickListener);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_send_feed_back, container, false);
        return view;
    }

    private void sendSendFeedback() {
        String message = etSendFeedback.getText().toString();
        if (StringUtil.isNullOrEmpty(message)) {
            etSendFeedback.setError(activity.getString(R.string.this_field_can_not_empty));
        }
        ContextUtil.raiseEmailIntent(activity, "Suggestion and Feedback", message, "");
    }

    View.OnClickListener sendFeedbackOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            sendSendFeedback();
        }
    };
}
