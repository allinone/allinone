package rajput.allinone.allinonefragemnt;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import pojos.WebCategory;
import pojos.WebLinkInstance;
import rajput.allinone.CategoryActivity;
import rajput.allinone.R;
import util.Constants;
import util.ListUtil;
import util.Util;
import webview.WebViewActivity;

/**
 * Created by sheelender on 29/10/15.
 */
public class WebCategoryFragmentHandler {


    private CategoryActivity activity;

    private LinearLayout main_layout;
    private ScrollView scrollView;
    private View fragmentView;


    public WebCategoryFragmentHandler(CategoryActivity activity, View view) {
        this.activity = activity;
        fragmentView = view;
        bindViews();
    }

    private void bindViews() {
        main_layout = (LinearLayout) fragmentView.findViewById(R.id.main_layout);
        scrollView = (ScrollView) fragmentView.findViewById(R.id.main_content);

    }

    public void initialize() {
        if (activity.baseResponseObject != null) {
            addHorizontalView(activity.baseResponseObject.getWebCategoryList());
        }
    }

    private void addHorizontalView(List<WebCategory> webCategoryList) {
        main_layout.removeAllViews();
        if (!ListUtil.isEmpty(webCategoryList)) {

            for (WebCategory webCategory : webCategoryList) {
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.holiday_recycler_view, null);
                LinearLayout llMainTagTitle = (LinearLayout) view.findViewById(R.id.llMainTagTitle);
                llMainTagTitle.setTag(webCategory.getCategoryName());
                llMainTagTitle.setOnClickListener(titleOnClickListener);
                TextView packageTitle = (TextView) view.findViewById(R.id.mainTagTitle);
                LinearLayout llHorizontalView = (LinearLayout) view.findViewById(R.id.llHorizontalView);
                packageTitle.setText(webCategory.getCategoryName());
                addHorizontalLayout(llHorizontalView, webCategory.getWebLinkInstances());
                main_layout.addView(view);
            }
        }
    }

    public void addHorizontalLayout(LinearLayout llHorizontalView, List<WebLinkInstance> webLinkInstances) {
        for (WebLinkInstance object : webLinkInstances) {

            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.holiday_horizontal_view, null);
            ImageView websiteImage = (ImageView) view.findViewById(R.id.websiteImage);
            TextView websiteName = (TextView) view.findViewById(R.id.websiteName);

            String wesiteName = object.getWebsiteName().toLowerCase().replace(" ", "_");
            int resId = activity.getResources().getIdentifier(wesiteName, "drawable", activity.getPackageName());
            if (resId != 0) {
                Picasso.with(activity.getApplicationContext()).load(resId).into(websiteImage);
            } else {
                websiteImage.setVisibility(View.GONE);
            }
            websiteName.setText(object.getWebsiteName());
            websiteImage.setOnClickListener(onClickListener);
            websiteImage.setTag(object);

            llHorizontalView.addView(view);

        }
    }

    OnClickListener titleOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            String categoryName = (String) view.getTag();
            WebCategory webCategory = activity.webCategoryHashMap.get(categoryName);
            startFragment(webCategory);
        }
    };
    OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            openWebViewActivity((WebLinkInstance) view.getTag());
        }
    };

    private void startFragment(WebCategory webCategory) {
        Fragment singleCategoryFragment = new SingleCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.WEB_LIST, Util.getJson(webCategory));
        singleCategoryFragment.setArguments(bundle);
        activity.reloadFragment(singleCategoryFragment);
    }

    private void openWebViewActivity(WebLinkInstance webLinkInstance) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(webLinkInstance);
        intent.putExtra("webLink", json);
        activity.startActivity(intent);
    }
}
