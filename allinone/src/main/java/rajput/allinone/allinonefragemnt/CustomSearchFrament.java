package rajput.allinone.allinonefragemnt;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import base.BaseDefaultActivity;
import rajput.allinone.R;
import webview.AllWebViewClient;


public class CustomSearchFrament extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View view;
    private WebView webView;
    private EditText etSearch;
    private Button btSearch;
    private BaseDefaultActivity activity;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public CustomSearchFrament() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CustomSearchFrament.
     */
    // TODO: Rename and change types and number of parameters
    public static CustomSearchFrament newInstance(String param1, String param2) {
        CustomSearchFrament fragment = new CustomSearchFrament();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_custom_search_frament, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //etSearch = (EditText) view.findViewById(R.id.etSearch);
        // btSearch = (Button) view.findViewById(R.id.bSearch);
        //btSearch.setOnClickListener(searchOnClickListener);
        webView = (WebView) view.findViewById(R.id.webview);
        activity = (BaseDefaultActivity) getActivity();
        setWebViewSetting();
        loadWebViewURL();

    }

    private void setWebViewSetting() {
        webView.setWebViewClient(new AllWebViewClient(activity));
        WebSettings webSettings = webView.getSettings();

        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webView.setWebChromeClient(new WebChromeClient());
        webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
    }

    private void loadWebViewURL() {
        webView.loadUrl("https://google.com");
    }

    View.OnClickListener searchOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onSearchClick();
        }
    };

    public void onSearchClick() {
        try {
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            String term = etSearch.getText().toString();
            intent.putExtra(SearchManager.QUERY, term);
            startActivity(intent);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }
}
