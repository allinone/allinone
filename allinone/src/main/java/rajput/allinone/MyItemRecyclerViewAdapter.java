package rajput.allinone;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import base.BaseDefaultActivity;
import pojos.WebLinkInstance;
import rajput.allinone.allinonefragemnt.SingleCategoryFragment.OnListFragmentInteractionListener;
import util.StringUtil;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<WebLinkInstance> mValues;
    private final OnListFragmentInteractionListener mListener;
    BaseDefaultActivity activity;

    public MyItemRecyclerViewAdapter(List<WebLinkInstance> items, OnListFragmentInteractionListener
            listener, Activity activity) {
        mValues = items;
        mListener = listener;
        this.activity = (BaseDefaultActivity) activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.webSiteName.setText(mValues.get(position).getWebsiteName());
        //  holder.webSiteDes.setText(mValues.get(position).getWebImage());
        String wesiteName = mValues.get(position).getWebsiteName().toLowerCase().replace(" ", "_");
        int resId = activity.getResources().getIdentifier(wesiteName, "drawable", activity.getPackageName());
        if (resId == 0) {
            holder.imageUrl.setVisibility(View.GONE);
        } else {
            holder.imageUrl.setImageResource(resId);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView webSiteName;
        public final TextView webSiteDes;
        public final ImageView imageUrl;
        public WebLinkInstance mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            webSiteName = (TextView) view.findViewById(R.id.webSiteName);
            webSiteDes = (TextView) view.findViewById(R.id.webSiteDes);
            imageUrl = (ImageView) view.findViewById(R.id.imageUrl);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + webSiteDes.getText() + "'";
        }
    }
}
