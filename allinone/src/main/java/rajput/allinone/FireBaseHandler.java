package rajput.allinone;

import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import pojos.WebCategory;
import pojos.WebResponseObject;
import rajput.allinone.allinonefragemnt.WebCategoryFragment;
import util.Constants;
import util.ListUtil;

/**
 * Created by sheelendar on 28/02/17.
 */

public class FireBaseHandler {
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private CategoryActivity activity;
    private String formId;

    public FireBaseHandler(CategoryActivity activity) {
        this.activity = activity;
        //    formId = Constants.MAIN_WEB_CATEGORIES;
        database = FirebaseDatabase.getInstance();
        //   myRef = database.getReference().child(formId);

    }

    public void fireBaseData() {


        // myRef.setValue("Hello, World!");
    }

    public void loadFireBaseForm(String formId) {
        this.formId = formId;
        myRef = database.getReference().child(formId);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                getFetchDataClass(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Toast.makeText(activity, error.toException().toString(), Toast.LENGTH_LONG).show();
                // Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void getFetchDataClass(DataSnapshot dataSnapshot) {
        switch (dataSnapshot.getKey()) {
            case Constants.MAIN_WEB_CATEGORIES:
                activity.baseResponseObject = dataSnapshot.getValue(WebResponseObject.class);
                initialize();
                WebCategoryFragment webCategoryFragment = new WebCategoryFragment();
                activity.reloadFragment(webCategoryFragment);
                activity.hideProgressBar();
                break;
            case Constants.OTHER_WEB_CATEGORIES:
                activity.otherWebCategory = dataSnapshot.getValue(WebCategory.class);
                break;
        }

    }

    public void initialize() {
        if (activity.baseResponseObject != null && !ListUtil.isEmpty(activity.baseResponseObject.getWebCategoryList())) {
            activity.webCategoryHashMap = new HashMap<>();
            for (WebCategory webCategory : activity.baseResponseObject.getWebCategoryList()) {
                activity.webCategoryHashMap.put(webCategory.getCategoryName(), webCategory);

            }
        }
    }
}
