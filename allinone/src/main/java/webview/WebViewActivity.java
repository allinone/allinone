package webview;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import base.BaseDefaultActivity;
import pojos.WebLinkInstance;
import rajput.allinone.R;
import util.Util;

/**
 * Created by sheelendar on 03/04/17.
 */

public class WebViewActivity extends BaseDefaultActivity {
    private WebLinkInstance webLinkInstance;
    private WebView myWebView;
    private ProgressBar webProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getDataFromIntent();

        myWebView = (WebView) findViewById(R.id.webview);
        webProgressBar = (ProgressBar) findViewById(R.id.webProgressBar);
        setWebViewSetting();
        loadWebViewURL();


    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();

        String objectJson = bundle.getString("webLink");
        webLinkInstance = Util.parseJson(objectJson, WebLinkInstance.class);

    }

   /* private void setWebViewSetting() {
        myWebView.setWebViewClient(new AllWebViewClient(this));
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setAllowFileAccess(true);

    }
*/
    private void setWebViewSetting() {
        myWebView.setWebViewClient(new AllWebViewClient(this));
        WebSettings webSettings = myWebView.getSettings();

        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        myWebView.setWebChromeClient(new WebChromeClient());
        webSettings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDatabaseEnabled(true);
    }

    private void loadWebViewURL() {
        myWebView.loadUrl(webLinkInstance.getWebLink());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (myWebView.canGoBack()) {
                    myWebView.goBack();
                    return true;
                }
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void hideProgressBar() {
        if (webProgressBar != null) {
            webProgressBar.setVisibility(View.GONE);
        }
    }

    public void showProgressBar() {
        if (webProgressBar != null) {
            webProgressBar.setVisibility(View.VISIBLE);
        }
    }
}
