package util;

/**
 * Created by sheelendar on 14/05/17.
 */

public final class Constants {
    public static final String WEB_LIST = "WEB_LIST";
    public static final String MAIN_WEB_CATEGORIES = "MAIN_WEB_CATEGORIES";
    public static final String OTHER_WEB_CATEGORIES = "OTHER_WEB_CATEGORIES";
    public static final String ALLINONE_CONTAINER_ID = "GTM-PGP7F6X";

}
