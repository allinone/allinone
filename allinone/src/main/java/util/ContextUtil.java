package util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import base.BaseDefaultActivity;
import rajput.allinone.R;

/**
 * Created by sheelendar on 13/05/17.
 */

public class ContextUtil {

    public static void showToast(Context context, String message, boolean isLong) {
        int length = Toast.LENGTH_SHORT;
        if (isLong) {
            length = Toast.LENGTH_LONG;
        }
        try {
            Toast.makeText(context.getApplicationContext(), message, length).show();
        } catch (Exception e) {
        }

    }

    public static void showSnackBar(Context context, int msgId) {
        try {
            if (context instanceof AppCompatActivity) {
                Snackbar.make(((AppCompatActivity) context).findViewById(android.R.id.content), msgId, Snackbar.LENGTH_LONG).show();
                return;
            }
            showToast(context, context.getString(msgId), true);
        } catch (Exception e) {
        }
    }

    public static Snackbar showSnackBar(Context context, String message, String buttonText, View.OnClickListener onClickListener) {
        try {
            Snackbar snackbar = Snackbar.make(((AppCompatActivity) context).findViewById(android.R.id.content),
                    Html.fromHtml(message), Snackbar.LENGTH_LONG)
                    .setAction(buttonText, onClickListener);
            snackbar.show();
            return snackbar;
        } catch (Exception e) {
        }
        return null;
    }

    public static void showSnackBar(Context context, String message) {
        try {
            if (StringUtil.isNullOrEmpty(message)) {
                return;
            }
            if (context instanceof AppCompatActivity) {
                Snackbar.make(((AppCompatActivity) context).findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
                return;
            }
            showToast(context, message, true);
        } catch (Exception e) {
        }

    }

    public static String getAppUrl(Context context, String suffix) {
        return "https://play.google.com/store/apps/details?id=" + context.getApplicationContext().
                getPackageName().toLowerCase();
    }

    public static void shareData(Context context, String subject, String data) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, data);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public static void raiseEmailIntent(Context ctx,
                                        String subject, String bodyText, String title) {

        String mailTo = "all.i.o.mobiles@gmail.com";
        String uriText = "mailto:" + mailTo + "?subject=" + Uri.encode(subject)
                + "&body=" + Uri.encode(bodyText);

        Uri uri = Uri.parse(uriText);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(uri);
        ctx.startActivity(Intent.createChooser(emailIntent, "Send email"));

    }

    public static boolean isNetworkAvailable(final BaseDefaultActivity activity, DialogInterface.OnClickListener
            okBtnClickListener) {
        try {


            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService
                    (Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return true;
            }
            showConfirmationAlert(activity, R.string.no_internet, R.string.internet_error_message,
                    R.string.dialog_ok, okBtnClickListener);

        } catch (Exception e) {
        }
        return false;
    }

    public static void showConfirmationAlert(final Context context, int title,
                                             int msg, int btnText,
                                             DialogInterface.OnClickListener okBtnClickListener) {
        try {
            AlertDialog.Builder searchDialog = new AlertDialog.Builder(context);
            searchDialog.setTitle(context.getString(title));
            searchDialog.setMessage(msg);
            searchDialog.setCancelable(true);
            searchDialog.setPositiveButton(btnText, okBtnClickListener);
            AlertDialog dialog = searchDialog.create();
            dialog.show();
        } catch (Exception e) {

        }
    }
}
