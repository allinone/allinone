package util;

import com.google.gson.Gson;

import pojos.BaseResponse;

/**
 * Created by sheelendar on 03/04/17.
 */

public class Util {

    /*public static String getJSON(Object obj) {
        try {
            Gson gson = new GsonWrapper(new AppFieldNamingStrategy()).buildGson();
            return gson.toJson(obj);
        } catch (Exception e) {
            return "";
        }

    }

    public static <T> T parseGson(Class<T> className, String json) {
        if (json == null) {
            return null;
        }
        try {
            Gson gson = new GsonWrapper(new AppFieldNamingStrategy()).buildGson(json, className);
            return gson.fromJson(json, className);
        } catch (Exception e) {
            try {
                Gson gson = new GsonWrapper(new AppFieldNamingStrategy()).buildGson();
                return gson.fromJson(json, className);
            } catch (Exception e1) {
                return null;
            }
        }
    }*/
    public static String getJson(BaseResponse response) {
        Gson gson = new Gson();
        String json = gson.toJson(response);
        return json;
    }

    public static <E> E parseJson(String json, Class<E> className) {

        Gson gson = new Gson();
        E object = gson.fromJson(json, className);
        return object;
    }
}
