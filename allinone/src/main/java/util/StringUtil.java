package util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StringUtil {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.trim().equals("") || s.trim().equalsIgnoreCase("null");
    }

    public static String replaceSpaces(String s) {
        if (s == null) {
            return null;
        }
        return s.trim().replaceAll(" ", "%20");
    }

    public static String trim(String s) {
        if (s == null) {
            return null;
        }
        return s.trim();
    }

    public static int parseInt(String s, int defaultValue) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static double parseDouble(String s, double defaultValue) {
        try {
            return Double.parseDouble(s);
        } catch (Exception e) {
            return defaultValue;
        }
    }


    public static boolean parseBoolean(String s, boolean defaultValue) {
        try {
            return Boolean.parseBoolean(s);
        } catch (Exception e) {
            return defaultValue;
        }
    }


    public static boolean isNullOrEmptyOrZero(String s) {
        return isNullOrEmpty(s) || s.trim().equals("0");
    }

    public static String getNullToZero(String s) {
        return isNullOrEmpty(s) ? "0" : s.trim();
    }

    public static String r(String s, String defaultString) {
        if (isNullOrEmpty(s)) {
            return defaultString;
        }
        return s;
    }

    public static String r(String s) {
        if (isNullOrEmpty(s)) {
            return "";
        }
        return s;
    }

    public static String r(String s, String defaultString, int len) {
        if (isNullOrEmpty(s)) {
            return defaultString;
        }
        if (len <= 0 || s.length() <= len) {
            return s;
        }
        return s.substring(0, len - 1);
    }

    public static String lower(String s) {
        if (isNullOrEmpty(s)) {
            return "";
        }
        return s.toLowerCase();
    }

    public static String display(String s) {
        if (isNullOrEmpty(s)) {
            return "";
        }
        return s.replace("_", " ");
    }

    public static String elipsize(String s, int length) {
        if (s.length() > length) {
            return s.substring(0, length - 1) + "...";
        }
        return s;
    }

    public static boolean isNumber(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {

        }
        return false;
    }

    public static String readable(String s) {
        s = s.replace("&quot;", "'");
        s = s.replace("&lsquo;", "'");
        s = s.replace("&rsquo;", "'");
        s = s.replace("&ndash;", "-");
        s = s.replace("&amp;", "&");
        return s;
    }

    public static String capitalizeEveryWord(String str, String delimeter) {
        if (isNullOrEmpty(str)) {
            return "";
        }
        str = str.toLowerCase();

        String[] words = str.trim().split(delimeter);
        String capitalizedString = "";
        for (String word : words) {
            if (!isNullOrEmpty(word))
                capitalizedString += " " + capitalize(word);
        }
        return capitalizedString.trim();
    }

    public static String replaceAll(String str, String delimeter, String desiredDelimeter) {
        str = str.trim();
        if (isNullOrEmpty(str)) {
            return "";
        }

        String[] words = str.split(delimeter);
        String capitalizedString = "";
        for (String word : words) {
            if (!isNullOrEmpty(word))
                capitalizedString += desiredDelimeter + capitalize(word);
        }
        return capitalizedString.trim();
    }

    public static String capitalize(String word) {
        if (isNullOrEmpty(word)) {
            return "";
        }

        return word.trim().substring(0, 1).toUpperCase() + word.trim().substring(1).toLowerCase();
    }

    public static String stringFromArray(String[] array, String delimeter) {
        String st = "-1";
        for (String str : array) {
            st += delimeter + str;
        }
        return st;
    }

    public static String replaceString(String targetString, String keyString, String defaultString) {
        if (isNullOrEmpty(targetString)) {
            return "";
        }
        return targetString.replace(keyString, defaultString);
    }

    public static String getReferAndEarnString(String targetString, String defaultEarnValue) {
        if (isNullOrEmpty(targetString)) {
            return "";
        }
        return (targetString.replace("refer_cash_value", defaultEarnValue)).replace("refer_point_name", "Points");
    }

    public static String readFully(InputStream inputStream, String encoding)
            throws IOException {
        return new String(readFully(inputStream), encoding);
    }

    private static byte[] readFully(InputStream inputStream)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos.toByteArray();
    }

    public static boolean isContainSpecialCharacterOrDigit(String targetString) {
        try {
            if (targetString.matches("[a-zA-Z ]*")) {
                return false;
            }
        } catch (Exception e) {
        }
        return true;
    }

    public static String getSubString(String str, int startIndex, int endIndex) {
        if (str.length() < endIndex) {
            return str;
        }

        return str.substring(startIndex, endIndex);
    }

    public static String getStringFromArrayStartingWithDelimeter(String[] array, String delimeter) {
        StringBuilder sb = new StringBuilder("");
        if (array != null && array.length > 0) {
            for (String str : array) {
                sb.append(delimeter).append(str);
            }
        }
        return sb.toString();
    }

    public static String getStringFromArrayStartingWithString(String[] array, String delimeter) {
        String preDelimeter = "";
        StringBuilder sb = new StringBuilder("");
        if (array != null && array.length > 0) {
            for (String str : array) {
                sb.append(preDelimeter).append(str);
                if (StringUtil.isNullOrEmpty(preDelimeter)) {
                    preDelimeter = delimeter;
                }
            }
        }
        return sb.toString();
    }


    public static String getStringFromList(List<String> list, String delimeter) {
        return getStringFromArrayStartingWithDelimeter(list.toArray(new String[list.size()]), delimeter);
    }

        public static String checkNullEmpty(String str) {
        return isNullOrEmpty(str) ? "" : str;
    }
}
