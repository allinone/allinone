package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ListUtil {

    public static <T> boolean isEmpty(Collection<T> l) {
        return l == null || l.size() == 0;
    }

    public static List getList(Object... objList) {
        List<Object> list = new ArrayList<Object>();
        for (Object obj : objList) {
            if (obj != null) {
                list.add(obj);
            }
        }
        return list;
    }

    public static <T> int size(Collection<T> List) {
        if (List == null || List.isEmpty()) {
            return 0;
        }
        return List.size();
    }

    public static <T> ArrayList<T> getArrayList(ArrayList<T> list, int startIndex, int lastIndex) {
        ArrayList<T> nlist = new ArrayList<T>();
        for (T item : list.subList(startIndex, lastIndex)) {
            nlist.add(item);
        }
        return nlist;

    }

    /*public static List<FPair> urlSplitter(String url) {
        if (StringUtil.isNullOrEmpty(url)) {
            return Collections.emptyList();
        }
        List<FPair> list = new ArrayList<>();
        String[] params = url.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] param = params[i].split("=");
            if (param.length == 2) {
                list.add(new FPair(param[0], param[1]));
            }
        }
        return list;
    }*/
}
