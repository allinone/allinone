package pojos;

import java.util.List;

import pojos.WebLinkInstance;

/**
 * Created by sheelendar on 28/02/17.
 */

public class WebResponseObject extends BaseResponse {
    private List<WebCategory> webCategoryList;

    public List<WebCategory> getWebCategoryList() {
        return webCategoryList;
    }

    public void setWebCategoryList(List<WebCategory> webCategoryList) {
        this.webCategoryList = webCategoryList;
    }
}
