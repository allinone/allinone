package pojos;

import java.util.List;

/**
 * Created by sheelendar on 04/03/17.
 */

public class WebCategory extends BaseResponse {
    private String categoryName;
    private String categoryImage;
    private List<WebLinkInstance> webLinkInstances;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public List<WebLinkInstance> getWebLinkInstances() {
        return webLinkInstances;
    }

    public void setWebLinkInstances(List<WebLinkInstance> webLinkInstances) {
        this.webLinkInstances = webLinkInstances;
    }
}
