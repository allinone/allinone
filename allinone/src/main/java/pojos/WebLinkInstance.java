package pojos;

import java.util.List;

/**
 * Created by sheelendar on 28/02/17.
 */

public class WebLinkInstance extends BaseResponse {
    private String webLink;
    private String websiteName;
    private String webImage;
    private List<SubWebLink> subWebLinks;

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getWebImage() {
        return webImage;
    }

    public void setWebImage(String webImage) {
        this.webImage = webImage;
    }

    public List<SubWebLink> getSubWebLinks() {
        return subWebLinks;
    }

    public void setSubWebLinks(List<SubWebLink> subWebLinks) {
        this.subWebLinks = subWebLinks;
    }
}
